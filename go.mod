module github.com/ihaiker/vik8s

go 1.15

require (
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/hashicorp/go-version v1.2.0
	github.com/ihaiker/cobrax v1.2.5
	github.com/ihaiker/ngx/v2 v2.0.6
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/kvz/logstreamer v0.0.0-20150507115422-a635b98146f0
	github.com/olekukonko/tablewriter v0.0.4
	github.com/peterh/liner v1.2.0
	github.com/pkg/sftp v1.11.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.4.0
	github.com/ugorji/go v1.1.4 // indirect
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
	gopkg.in/VividCortex/ewma.v1 v1.1.1 // indirect
	gopkg.in/cheggaaa/pb.v2 v2.0.7 // indirect
	gopkg.in/fatih/color.v1 v1.7.0
	gopkg.in/mattn/go-colorable.v0 v0.1.0 // indirect
	gopkg.in/mattn/go-isatty.v0 v0.0.4 // indirect
	gopkg.in/mattn/go-runewidth.v0 v0.0.4 // indirect
	k8s.io/api v0.18.5
	k8s.io/apiextensions-apiserver v0.18.5
	k8s.io/apimachinery v0.18.5
	k8s.io/client-go v0.18.5
	k8s.io/utils v0.0.0-20200619165400-6e3d28b6ed19 // indirect
	sigs.k8s.io/yaml v1.2.0
)
